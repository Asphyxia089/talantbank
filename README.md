<h2>Установка и запуск</h2>

<p>1. Установка с <a href="https://www.npmjs.com/" target="_blank">npm</a></p>

<code>npm i</code>

<p>Запуск: <code>npm run dev</code></p>


<p>2. Установка с <a href="https://yarnpkg.com/" target="_blank">yarn</a></p>

<code>yarn</code>

<p>Запуск: <code>yarn dev</code></p>

