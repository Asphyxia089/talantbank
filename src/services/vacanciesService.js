import axios from 'axios';

export const VacanciesService = {
	async getVacancies() {
		const response = await axios({
			method: 'get',
			url: 'http://158.160.32.12:8000/list-vacancies/',
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
			},
		});
		return response.data;
	},
	// async deleteVacancy(id) {
	// 	const response = await axios.delete(
	// 		`http://localhost:3000/list-vacancies/${id}`
	// 	);
	// 	return response.data;
	// },
	// async duplicateVacancy(vacancy) {
	// 	const response = await axios.post(
	// 		`http://localhost:3000/list-vacancies`,
	// 		vacancy
	// 	);
	// 	return response.data;
	// },
};
