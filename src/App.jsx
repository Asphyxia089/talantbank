import React from 'react';
import {
	BrowserRouter as Router,
	Routes,
	Route,
	Navigate,
} from 'react-router-dom';

import Vacancies from './components/Vacancies/Vacancies';

import Sidebar from './components/Sidebar/Sidebar';
import Login from './components/Login/Login';
import Candidates from './components/Candidates/Candidates';

const App = () => {
	return (
		<Router>
			<Routes>
				<Route path='/' element={<Navigate to='/vacancies' />} />
				<Route path='/login' element={<LoginWithNoSidebar />} />
				<Route path='/vacancies' element={<VacanciesWithSidebar />} />
				<Route path='/candidates' element={<CandidatesWithSidebar />} />
			</Routes>
		</Router>
	);
};

const LoginWithNoSidebar = () => {
	return <Login />;
};

const VacanciesWithSidebar = () => {
	return (
		<div className='page'>
			<Sidebar />
			<Vacancies />
		</div>
	);
};
const CandidatesWithSidebar = () => {
	return (
		<div className='page'>
			<Sidebar />
			<Candidates />
		</div>
	);
};

export default App;
