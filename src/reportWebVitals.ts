import { Metric, getCLS, getFID, getFCP, getLCP, getTTFB } from 'web-vitals';

const reportWebVitals = (
	onPerfEntry?: ((metric: Metric) => void) | undefined
) => {
	if (onPerfEntry && typeof onPerfEntry === 'function') {
		import('web-vitals').then((module) => {
			const { getCLS, getFID, getFCP, getLCP, getTTFB } = module;
			getCLS(onPerfEntry);
			getFID(onPerfEntry);
			getFCP(onPerfEntry);
			getLCP(onPerfEntry);
			getTTFB(onPerfEntry);
		});
	}
};

export default reportWebVitals;
