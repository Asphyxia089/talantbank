import { NavLink, Link } from 'react-router-dom';

const Sidebar = () => {
	return (
		<div className='page__sidebar sidebar'>
			<div className='sidebar__logo'>
				<Link to={'/vacancies'}>
					<img src='/logo.svg' alt='logo' />
				</Link>
			</div>
			<nav className='sidebar__body'>
				<ul className='sidebar__list'>
					<li className='sidebar__item'>
						<NavLink
							className={'sidebar__link'}
							style={({ isActive }) => ({
								background: isActive ? '#e0e8f4' : '',
							})}
							to='/requests'
						>
							<img src='/requests.svg' alt='requests' />
							<div className='sidebar__text'>Заявки</div>
						</NavLink>
					</li>
					<li className='sidebar__item'>
						<NavLink
							className={'sidebar__link'}
							style={({ isActive }) => ({
								background: isActive ? '#e0e8f4' : '',
							})}
							to='/vacancies'
						>
							<img src='/vacancies.svg' alt='requests' />
							<div className='sidebar__text'>Вакансии</div>
						</NavLink>
					</li>
					<li className='sidebar__item'>
						<NavLink
							className={'sidebar__link'}
							style={({ isActive }) => ({
								background: isActive ? '#e0e8f4' : '',
							})}
							to='/candidates'
						>
							<img src='/candidates.svg' alt='requests' />
							<div className='sidebar__text'>Кандидаты</div>
						</NavLink>
					</li>
					<li className='sidebar__item'>
						<NavLink
							className={'sidebar__link sidebar__link_search'}
							to='/supersearch'
							style={({ isActive }) => ({
								background: isActive ? '#e0e8f4' : '',
							})}
						>
							<img src='/supersearch.svg' alt='requests' />
							<div className='sidebar__text'>
								Супер <br></br> Поиск
							</div>
						</NavLink>
					</li>
					<li className='sidebar__item'>
						<NavLink
							className={'sidebar__link'}
							style={({ isActive }) => ({
								background: isActive ? '#e0e8f4' : '',
							})}
							to='/reports'
						>
							<img src='/reports.svg' alt='requests' />
							<div className='sidebar__text'>Отчёты</div>
						</NavLink>
					</li>
					<li className='sidebar__item'>
						<NavLink
							className={'sidebar__link'}
							style={({ isActive }) => ({
								background: isActive ? '#e0e8f4' : '',
							})}
							to='/calendar'
						>
							<img src='/calendar.svg' alt='requests' />
							<div className='sidebar__text'>Календарь</div>
						</NavLink>
					</li>
					<li className='sidebar__item'>
						<NavLink
							className={'sidebar__link'}
							style={({ isActive }) => ({
								background: isActive ? '#e0e8f4' : '',
							})}
							to='/settings'
						>
							<img src='/settings.svg' alt='requests' />
							<div className='sidebar__text'>Настройки</div>
						</NavLink>
					</li>
				</ul>
			</nav>
			<div className='sidebar__footer'>
				<button className='sidebar__info-btn'>
					<img src='/infobtn.svg' alt='info' />
				</button>
				<button className='sidebar__alert-btn'>
					<img src='/alertbtn.svg' alt='alert' />
				</button>
				<NavLink to='/profile'>
					<img
						className='sidebar__profile-avatar'
						src='/avatar.png'
						alt='profile'
					/>
				</NavLink>
			</div>
		</div>
	);
};

export default Sidebar;
