import { useState } from 'react';

const Login = () => {
	const [login, setLogin] = useState('');
	const [password, setPassword] = useState('');
	const [showPassword, setShowPassword] = useState(false);
	const [forgotPassword, setForgotPassword] = useState(false);

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const body = JSON.stringify({
				login,
				password,
			});

			const response = await fetch(
				'http://158.160.32.12:8000/api-auth/login/',
				{
					method: 'POST',
					headers: {
						'Access-Control-Allow-Origin': '*',
						'Content-Type': 'application/json',
					},
					body,
				}
			);

			const data = await response.json();
			console.log(data);

			setLogin('');
			setPassword('');
		} catch (error) {
			console.log(error);
		}
	};

	const handlePasswordChange = (e) => {
		const value = e.target.value;
		if (value.length <= 8) {
			setPassword(value);
		}
	};

	const handleTogglePasswordVisibility = () => {
		setShowPassword(!showPassword);
	};

	const handleForgotPassword = () => {
		setForgotPassword(true);
	};
	const handleReturnToLogin = () => {
		setForgotPassword(false);
	};

	return (
		<div className='page__signin signin'>
			<div className='signin__container'>
				<div className='signin__body'>
					<div className='signin__header'>
						<img src='/logoTalant.png' alt='logo' />
					</div>
					<div className='signin__content'>
						{forgotPassword ? (
							<form onSubmit={handleForgotPassword}>
								<h1 className='signin__reset-title title'>Забыли пароль?</h1>
								<p className='signin__reset-text'>
									Чтобы сбросить пароль, введите свой адрес электронной почты.
									Вы получите электронное письмо для установки нового пароля
								</p>
								<label htmlFor='login' className='signin__label'>
									Корпоративная эл. почта
								</label>
								<div className='signin__input-container'>
									<input
										className='signin__input'
										type='text'
										name='username'
										placeholder='example@sovcombank.ru'
										autoComplete='off'
										required
									/>
								</div>
								<div className='signin__footer signin__footer-reset-container'>
									<button type='submit' className='signin__btn'>
										Отправить
									</button>
									<div className='signin__footer-reset-password'>
										<div>Вспомнили пароль?</div>
										<button
											onClick={handleReturnToLogin}
											className='signin__footer-text'
										>
											Войти
										</button>
									</div>
								</div>
							</form>
						) : (
							<form onSubmit={handleSubmit}>
								<label htmlFor='login' className='signin__label'>
									Корпоративная эл. почта
								</label>
								<div className='signin__input-container'>
									<input
										className='signin__input'
										type='text'
										name='username'
										required
										placeholder='example@sovcombank.ru'
										value={login}
										onChange={(e) => setLogin(e.target.value)}
										autoComplete='off'
									/>
								</div>
								<label htmlFor='password' className='signin__label'>
									Пароль
								</label>
								<div className='signin__input-container'>
									<input
										className='signin__input'
										type={showPassword ? 'text' : 'password'}
										name='password'
										placeholder='8 символов'
										required
										value={password}
										onChange={handlePasswordChange}
										autoComplete='off'
									/>
									<button
										className='signin__viewpass'
										onClick={handleTogglePasswordVisibility}
									>
										<img src='/viewPass.svg' alt='view pass' />
									</button>
								</div>
								<div className='signin__not-reg'>
									Если вы ещё не зарегистрированы, получите доступ у
									администраторов вашей организации
								</div>
								<div className='signin__footer'>
									<button type='submit' name='submit' className='signin__btn'>
										Войти
									</button>
									<button
										className='signin__footer-text'
										onClick={handleForgotPassword}
									>
										Не помню пароль
									</button>
								</div>
							</form>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default Login;
