import { useEffect, useState } from 'react';

import { VacanciesService } from '../../services/vacanciesService';
import SearchForm from './SearchForm/SearchForm';
import moment from 'moment/min/moment-with-locales';

const Vacancies = () => {
	const [vacancies, setVacancies] = useState([]);
	const [filteredVacancies, setFilteredVacancies] = useState([]);
	const [searchQuery, setSearchQuery] = useState('');
	const [currentPage, setCurrentPage] = useState(1);
	const [totalPages, setTotalPages] = useState(1);
	const [rowsPerPage] = useState(6);
	const [isSortDropdownOpen, setIsSortDropdownOpen] = useState(false);
	const [selectedSortOption, setSelectedSortOption] = useState(
		'Сначала недавно обновлённые'
	);
	const [isFilterModalOpen, setIsFilterModalOpen] = useState(false);
	const [vacancyCount, setVacancyCount] = useState(0);

	moment.locale('ru');

	const toggleFilterModal = () => {
		setIsFilterModalOpen((prevState) => !prevState);
	};

	useEffect(() => {
		const fetchData = async () => {
			const data = await VacanciesService.getVacancies();
			setVacancies(data);
			setFilteredVacancies(data);
			setTotalPages(Math.ceil(data.length / rowsPerPage));
			setVacancyCount(data.length);
		};

		fetchData();
	}, [rowsPerPage]);

	useEffect(() => {
		const startIndex = (currentPage - 1) * rowsPerPage;
		const endIndex = startIndex + rowsPerPage;
		setFilteredVacancies(vacancies.slice(startIndex, endIndex));
	}, [currentPage, rowsPerPage, vacancies]);

	const handleSearch = (searchQuery) => {
		setSearchQuery(searchQuery);

		if (searchQuery === '') {
			setFilteredVacancies(vacancies);
		} else {
			const filtered = vacancies.filter((vacancy) =>
				vacancy.name.toLowerCase().includes(searchQuery.toLowerCase())
			);
			setFilteredVacancies(filtered);
		}
	};

	const [isOpen, setIsOpen] = useState([]);
	const toggleDropdown = (index) => {
		const newIsOpen = [...isOpen];
		newIsOpen[index] = !newIsOpen[index];
		setIsOpen(newIsOpen);
	};

	// const handleDelete = async (id) => {
	// 	try {
	// 		await VacanciesService.deleteVacancy(id);
	// 		const updatedVacancies = await VacanciesService.getVacancies();
	// 		setVacancies(updatedVacancies);
	// 		setTotalPages(Math.ceil(updatedVacancies.length / rowsPerPage));

	// 		const filtered = updatedVacancies.filter((vacancy) =>
	// 			vacancy.name.toLowerCase().includes(searchQuery.toLowerCase())
	// 		);
	// 		setFilteredVacancies(filtered);

	// 		if (filtered.length === 0 && searchQuery !== '') {
	// 			setSearchQuery('');
	// 			setFilteredVacancies(updatedVacancies);
	// 		}
	// 	} catch (error) {
	// 		console.error('Ошибка при удалении:', error);
	// 	}
	// };

	// const handleDuplicate = (vacancyId) => {
	// 	const duplicatedVacancy = vacancies.find(
	// 		(vacancy) => vacancy.id === vacancyId
	// 	);
	// 	if (duplicatedVacancy) {
	// 		const newVacancy = { ...duplicatedVacancy };
	// 		const lastId = vacancies[vacancies.length - 1]?.id || 0;
	// 		newVacancy.id = lastId + 1;
	// 		setVacancies((prevVacancies) => [...prevVacancies, newVacancy]);
	// 		setFilteredVacancies((prevVacancies) => [...prevVacancies, newVacancy]);
	// 		VacanciesService.duplicateVacancy(newVacancy);
	// 	}
	// };

	const handlePageChange = (pageNumber) => {
		setCurrentPage(pageNumber);
	};
	const toggleSortDropdown = () => {
		setIsSortDropdownOpen((prevState) => !prevState);
	};

	const handleSortOptionClick = (option) => {
		setSelectedSortOption(option);
		setIsSortDropdownOpen(false);
		if (option === 'Сначала недавно обновлённые') {
			// Sort by recent update logic
		} else if (option === 'Сначала давно не обновлявшиеся') {
			// Sort by long time not updated logic
		}
	};

	return (
		<div className='page__main main'>
			<section className='main__vacancies vacancies'>
				<header className='vacancies__header header'>
					<div className='header__body'>
						<h1 className='header__title title'>Вакансии</h1>
						<button className='header__btn btn'>
							<div>Создать</div>
							<img src='/plus.svg' alt='' />
						</button>
					</div>
				</header>
				<SearchForm onSearch={handleSearch} searchQuery={searchQuery} />
				<div className='vacancies__filter filter'>
					<button className='filter__btn' onClick={toggleFilterModal}>
						<img src='/filter.svg' alt='' />
						<div className='filter__text'>Фильтры</div>
					</button>
					{isFilterModalOpen && (
						<div className='filter__overlay'>
							<div className='filter__modal'>
								<button
									className='filter__modal-close'
									onClick={toggleFilterModal}
								>
									<img src='/close.svg' alt='Закрыть'></img>
								</button>
								<div className='filter__modal-body'>
									<nav className='filter__modal-header'>
										<h1 className='filter__modal-title title'>Фильтры</h1>
									</nav>

									<div className='filter__modal-content'>
										<div className='filter__modal-item'>
											<label className='filter__modal-text'>Вакансия</label>
											<div className='filter__modal-dropdown'>
												<input
													className='filter__modal-dropdown-btn'
													placeholder='Все вакансии'
												></input>
											</div>
										</div>
										<div className='filter__modal-item'>
											<label className='filter__modal-text'>Рекрутер</label>
											<div className='filter__modal-dropdown'>
												<input
													className='filter__modal-dropdown-btn'
													placeholder='Полина Заболоцкая (я)'
												></input>
											</div>
										</div>
										<div className='filter__modal-item'>
											<label className='filter__modal-text'>Город</label>
											<div className='filter__modal-dropdown'>
												<input
													className='filter__modal-dropdown-btn'
													placeholder='Все города'
												></input>
											</div>
										</div>
										<div className='filter__modal-item'>
											<label className='filter__modal-text'>Статус</label>
											<div className='filter__modal-dropdown'>
												<input
													className='filter__modal-dropdown-btn'
													placeholder='Все статусы'
												></input>
											</div>
										</div>
										<div className='filter__modal-item'>
											<label className='filter__modal-text'>Заказчик</label>
											<div className='filter__modal-dropdown'>
												<input
													className='filter__modal-dropdown-btn'
													placeholder='Все'
												></input>
											</div>
										</div>
										<div className='filter__modal-itehm'>
											<label className='filter__modal-text'>Отдел</label>
											<div className='filter__modal-dropdown'>
												<input
													className='filter__modal-dropdown-btn'
													placeholder='Все отделы'
												></input>
											</div>
										</div>
										<div className='filter__modal-item'>
											<label className='filter__modal-text'>
												Дата создания вакансии
											</label>
											<div className='filter__modal-date-wrapper'>
												<div className='filter__modal-date-input'>
													<span className=' filter__modal-text-date'>От</span>
													<input
														type='date'
														className='filter__modal-date'
														name='from'
													></input>
												</div>
												<div className='filter__modal-date-input'>
													<span className='filter__modal-text-date'>До</span>
													<input
														type='date'
														className='filter__modal-date '
														name='to'
													></input>
												</div>
											</div>
										</div>
									</div>
									<div className='filter__modal-footer'>
										<button
											type='button'
											className='filter__modal-submit-btn btn'
										>
											Применить
										</button>
										<button type='button' className='filter__modal-reset-btn'>
											Сбросить все фильтры
										</button>
									</div>
								</div>
							</div>
						</div>
					)}
					<div className='filter__dropdown'>
						<button className='filter__btn' onClick={toggleSortDropdown}>
							<img src='/sort.svg' alt='' />
							<div className='filter__text'>{selectedSortOption}</div>
						</button>
						{isSortDropdownOpen && (
							<ul className='filter__dropdown-menu'>
								<div className='filter__dropdown-title'>Сортировка</div>
								<li className='filter__dropdown-item'>
									<button
										className='filter__dropdown-btn'
										onClick={() =>
											handleSortOptionClick('Сначала недавно обновлённые')
										}
									>
										Сначала недавно обновлённые
									</button>
								</li>
								<li className='filter__dropdown-item'>
									<button
										className='filter__dropdown-btn'
										onClick={() =>
											handleSortOptionClick('Сначала давно не обновлявшиеся')
										}
									>
										Сначала давно не обновлявшиеся
									</button>
								</li>
							</ul>
						)}
					</div>
				</div>
				{vacancies.length === 0 ? (
					<div className='vacancies__no-results'>Нет доступных вакансий</div>
				) : filteredVacancies.length === 0 ? (
					<div className='vacancies__no-results'>
						Не найдено результатов для "{searchQuery}"
					</div>
				) : (
					<div className='vacancies__table table'>
						<div className='table__row_header'>
							<div className='table__cell table__cell_vacancies'>
								{vacancyCount} Вакансии
							</div>
							<div className='table__cell table__cell_total'>Всего</div>
							<div className='table__cell table__cell_new'>Новые</div>
							<div className='table__cell table__cell_status'>Статус</div>
							<div className='table__cell table__cell_recruiter'>Рекрутер</div>
							<div className='table__cell table__cell_client'>Заказчик</div>
							<div className='table__cell table__cell_department'>Отдел</div>
							<div className='table__cell table__cell_city'>Город</div>
							<div className='table__cell table__cell_dataofcreation'>
								Дата создания
							</div>
						</div>
						{filteredVacancies.map((vacancy, index) => (
							<div className='table__row' key={vacancy.id}>
								<div className='table__more'>
									<button onClick={() => toggleDropdown(index)}>
										<img src='/more.svg' alt='' />
									</button>
									<div className='table__dropdown'>
										{isOpen[index] && (
											<ul className='table__dropdown-menu'>
												<li className='table__dropdown-text'>
													<button className='table__dropdown-btn'>
														Редактировать
													</button>
												</li>
												<li className='table__dropdown-text'>
													<button
														className='table__dropdown-btn'
														onClick={() => handleDuplicate(vacancy.id)}
													>
														Дублировать
													</button>
												</li>
												<li className='table__dropdown-text'>
													<button
														className='table__dropdown-btn'
														onClick={() => handleDelete(vacancy.id)}
													>
														Удалить
													</button>
												</li>
											</ul>
										)}
									</div>
								</div>
								<div className='table__cell table__cell_vacancies'>
									{vacancy.title}
								</div>
								<div className='table__cell table__cell_total'>
									{vacancy.total}
								</div>
								<div className='table__cell table__cell_new'>{vacancy.new}</div>
								<div className='table__cell table__cell_status'>
									<button className='table__btn'>
										<div className='table__btn-text'>{vacancy.status}</div>
										<img src='/closedlist.svg' alt=''></img>
									</button>
								</div>
								<div className='table__cell table__cell_recruiter'>
									{vacancy.recruiter}
									<br></br>
									<span className='table__role'>{vacancy.role}</span>
								</div>
								<div className='table__cell table__cell_client'>
									{vacancy.client}
									<br></br>
									<span className='table__role'>{vacancy.clientRole}</span>
								</div>
								<div className='table__cell table__cell_department'>
									{vacancy.department}
								</div>
								<div className='table__cell table__cell_city'>
									{vacancy.city}
								</div>
								<div className='table__cell table__cell_dataofcreation'>
									{moment(vacancy.created_at).format('LL')}
								</div>
							</div>
						))}
					</div>
				)}
			</section>
			<div className='vacancies__navbar'>
				{totalPages > 1 && (
					<div className='navbar__pagination pagination'>
						<button
							className={`pagination__btn ${
								currentPage === 1 ? 'pagination__btn--disabled' : ''
							}`}
							disabled={currentPage === 1}
							onClick={() => handlePageChange(currentPage - 1)}
						>
							<img src='/arrowLeft.svg' alt='' />{' '}
							<div className='pagination__text'>Назад</div>
						</button>
						<button
							className={`pagination__btn ${
								currentPage === totalPages ? 'pagination__btn--disabled' : ''
							}`}
							disabled={currentPage === totalPages}
							onClick={() => handlePageChange(currentPage + 1)}
						>
							<div className='pagination__text'>Вперед</div>
							<img src='/arrowRight.svg' alt='' />
						</button>
					</div>
				)}
			</div>
		</div>
	);
};

export default Vacancies;
