import React, { useState } from 'react';

const SearchForm = ({ onSearch }) => {
	const [searchQuery, setSearchQuery] = useState('');
	const [isChecked1, setIsChecked1] = useState(false);
	const [isChecked2, setIsChecked2] = useState(false);

	const handleSearchChange = (e) => {
		const query = e.target.value;
		setSearchQuery(query);

		if (query === '') {
			onSearch(''); // Reset the search
		}
	};

	const handleSearchSubmit = (e) => {
		e.preventDefault();
		onSearch(searchQuery);
	};

	const handleCheckboxChange1 = () => {
		setIsChecked1(!isChecked1);
		console.log('1');
	};
	const handleCheckboxChange2 = () => {
		setIsChecked2(!isChecked2);
		console.log('2');
	};
	return (
		<div className='vacancies__search'>
			<form className='vacancies__search-form' onSubmit={handleSearchSubmit}>
				<div className='vacancies__search-bar'>
					<div className='vacancies__search-icon'>
						<img src='/search.svg' alt=''></img>
					</div>
					<input
						type='text'
						value={searchQuery}
						onChange={handleSearchChange}
						placeholder='Поиск'
						name='search'
						className='vacancies__search-input'
					/>
				</div>
				<button className='vacancies__search-btn' type='submit'>
					Найти
				</button>
			</form>
			<div className='vacancies__checkboxes'>
				<div className='vacancies__checkbox-item'>
					<input
						type='checkbox'
						id='checkbox1'
						checked={isChecked1}
						onChange={handleCheckboxChange1}
						className='vacancies__checkbox'
					/>
					<label htmlFor='checkbox1' className='vacancies__checkbox-text'>
						Показать только мои вакансии
					</label>
				</div>
				<div className='vacancies__checkbox-item'>
					<input
						type='checkbox'
						id='checkbox2'
						checked={isChecked2}
						onChange={handleCheckboxChange2}
						className='vacancies__checkbox'
					/>
					<label htmlFor='checkbox2' className='vacancies__checkbox-text'>
						Показать закрытые вакансии
					</label>
				</div>
			</div>
		</div>
	);
};

export default SearchForm;
